
from os.path import join
from settings import VERBOSITY


class BaseClassifier(object):
	"""
		Chooser is a base class that can be overridden by e.g. a neural network or support vector machine. This allows for different methods and different params at each node in the species.

		Each instance gets it's configuration from a .json file in species/chooser/ .
	"""

	defaults = {}

	def __init__(self, node, **params):
		"""
			Do configuration in .conf().
		"""
		from species.tree_node import TreeNode
		assert isinstance(node, TreeNode)
		self.node = node
		""" Set all the params as properties. If you want to do something else, override __init__ in subclasses. """
		self.params = {}
		for key, val in self.defaults.items():
			self.params[key] = val
			setattr(self, key, val)
		for key, val in params.items():
			self.params[key] = val
			setattr(self, key, val)
		if VERBOSITY >= 3 and params:
			print '{0} overrides defaults for "{1}"'.format(self.node, '", "'.join(params.keys()))
		self.params_provided = len(params)
		self.unsaved_changed = False

	def __repr__(self):
		name = self.__class__.__name__.lower().replace('classifier', '')
		name += str(self.params_provided or '?')
		if self.unsaved_changed: name += '*'
		return name

	def load(self):
		"""
			Load the current training state. Continue if no save file is found.
		"""
		from settings import CLASSIFIER_STATE_DIR
		pth = join(CLASSIFIER_STATE_DIR, self.node.name)
		raise NotImplementedError('Override in subclasses.')

	def save(self):
		"""
			Save the current training state.

			Make sure that params that invalidate the state are part of the filename.
			E.g. the learning rate can change between runs, but the neural network size can not.
		"""
		from settings import CLASSIFIER_STATE_DIR
		pth = join(CLASSIFIER_STATE_DIR, self.node.name)
		self.unsaved_changed = False
		raise NotImplementedError('Override in subclasses.')

	def train(self, samples):
		"""
			Train this classifier with a given data set for each option.

			Note that this could be the same as the data from::
				{opt: opt.descendant_samples for opt in self.options}
			but it doesn't need to be (in case some training data wasn't used).

			:param samples: a list of KnownSample objects to train (can be assumed to be shuffled).
			:return: if not None, it should return the training RMSE (numpy array with a value for each output node)
		"""
		raise NotImplementedError('Override in subclasses.')

	def classify(self, samples):
		"""
			Given a list of samples without class specified, assign a probability for each class.

			:param sample: an UnknownSample object.
			:return: a normalized array of probabilities.
		"""
		raise NotImplementedError('Override in subclasses.')

	def classify_assign(self, samples):
		"""
			The same as classify, but add the probability to the sample instances.
		"""
		probabilities_list = self.classify(samples = samples)
		for sample, probabilities in zip(samples, probabilities_list):
			assert abs(sum(probabilities) - 1) < 1e-5
			for option, probability in zip(self.options, probabilities):
				sample.step_probability[option] = probability
		probabilities_list.append(probabilities)

	class ClassifierConfError(Exception): pass




