
from os.path import join
from simplejson import loads, scanner
from classifiers.classifier import BaseClassifier


def match_classifier(node):
	"""
		Finds the classifier config file for a specific node, then instantiates the corresponding classifier with the provided params. Also adds the classifier to the node.
		:param node: The node to which this classifier belongs.
	"""
	from settings import CLASSIFIERS, CLASSIFIER_CONF_DIR
	pth = join(CLASSIFIER_CONF_DIR, node.name + '.json')
	try:
		with open(pth) as fh:
			conf = loads(fh.read())
	except IOError:
		conf = {'method': 'default'}
	except scanner.JSONDecodeError as err:
		raise BaseClassifier.ClassifierConfError('configuration file "{0:s}" is not valid json:\n{1:s}'.format(pth, err))
	params = conf.get('params', {})
	try:
		classifier_name = conf['method']
	except KeyError:
		raise BaseClassifier.ClassifierConfError('no method specified in "{0:s}" for {1:s}'.format(pth, node.name))
	try:
		classifier_cls = CLASSIFIERS(classifier_name)
	except KeyError:
		raise BaseClassifier.ClassifierConfError('no method named "{0:s}"; see settings.CLASSIFIERS'.format(classifier_name))
	options = [child for child in node.children]
	if len(options) <= 1:
		return None
	classifier = classifier_cls(node, options = options, **params)
	classifier.options = options
	return classifier


