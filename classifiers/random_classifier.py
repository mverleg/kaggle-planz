
from numpy import ones
from os.path import join
from classifiers.classifier import BaseClassifier


class RandomClassifier(BaseClassifier):
	"""
		For testing purposes, this chooser picks some sample at 'random' but repeatable way (so it's not really random).
	"""

	defaults = {'offset': 0, 'froink': 'bliep'}

	@property
	def filename(self):
		from settings import CLASSIFIER_STATE_DIR
		return join(CLASSIFIER_STATE_DIR, '%s_v2_%s.rndstate' % (self.node.name, str(self.froink)))

	def load(self):
		from settings import VERBOSITY
		try:
			with open(self.filename, 'r') as fh:
				self.offset = int(fh.read())
				if VERBOSITY >= 3:
					print 'loaded classifier %s, offset = %d' % (self, self.offset)
		except IOError:
			if VERBOSITY >= 3:
				print 'classifier %s could not load, no state file found at %s' % (self, self.filename)

	def save(self):
		from settings import VERBOSITY
		try:
			with open(self.filename, 'w+') as fh:
				fh.write(str(int(self.offset)))
				if VERBOSITY >= 3:
					print 'saved classifier %s, offset = %d' % (self, self.offset)
		except IOError:
			if VERBOSITY >= 3:
				print 'classifier %s could not load, no state file found at %s' % (self, self.filename)
		self.unsaved_changed = False

	def train(self, samples):
		"""
			There is an offset, which has no real meaning but potentially changes the outcome after training (also predictably).
		"""
		from settings import VERBOSITY
		self.unsaved_changed = True
		self.offset = 0
		for k, sample in enumerate(samples):
			self.offset += int(255 * sum(sample.get_image()))
			if VERBOSITY >= 3 and k % 100 == 0:
				print ' %s trained 100 samples...' % self.node.name
		self.offset %= len(self.options)
		if VERBOSITY >= 2:
			print '%s "trained" by setting offset to %d' % (self.node.name, self.offset)

	def classify(self, samples):
		"""
			The winner is determined by the modulo of the sum of the data, which is repeatable but has no real meaning.
		"""
		from settings import VERBOSITY
		probabilities_list = []
		for sample in samples:
			winner = (int(sum(sample.get_image()) * 255) + self.offset) % len(self.options)
			probabilities = ones((len(self.options),))
			probabilities[winner] += 3.
			if VERBOSITY >= 3:
				print '%s classified pseudo-randomly (%d+%d %% %d)' % (self.node.name, sum(sample.get_image()), self.offset, len(self.options))
			probabilities_list.append(probabilities / sum(probabilities))
		return probabilities_list


