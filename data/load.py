
"""
	Load the data as sample objects.
"""

from os import listdir
#from random import shuffle, seed as set_seed, setstate, getstate
from os.path import join, basename
from data.sample import UnknownSample
from settings import VERBOSITY, PROCESSED_TESTING_IMAGES_DIR, TESTING_FEATURES_DIR
from species.tree_node import TreeNode


def get_training_data():
	"""
		Load the training data as dictionary of sample objects,

		No longer in deterministic order!
	"""
	samples = {node: node.samples for node in TreeNode.nodes.values()}
	if VERBOSITY >= 2:
		print 'returning %d samples without known classes' % len(samples)
	return samples
	# if KnownSample.all_nodes:
	# 	if not getattr(KnownSample, '_all_root', None) is root:
	# 		raise Exception('Samples were loaded twice with different root; this is not currently supported.')
	# 	return KnownSample.all_nodes
	# KnownSample._all_root = root
	# cnt = 0
	# for node in breadth_first(root):
	# 	#KnownSample.all[node] = [KnownSample(imagepath, featurepath, node) for imagepath, featurepath in zip(node.images, node.features)]
	# 	KnownSample.all[node] = node.samples
	# 	rand_state = getstate()
	# 	set_seed(seed + 7)
	# 	shuffle(KnownSample.all[node])
	# 	setstate(rand_state)
	# 	#node.samples = KnownSample.all[node]
	# 	cnt += len(KnownSample.all[node])
	# if VERBOSITY >= 3:
	# 	print 'created KnownSample objects for %d files' % cnt
	# return KnownSample.all


def get_testing_data():
	"""
		Load the data from unknown classes a sorted list. No mirroring used.
	"""
	images = [join(PROCESSED_TESTING_IMAGES_DIR, pth) for pth in listdir(PROCESSED_TESTING_IMAGES_DIR)]
	# changed from npy to png
	features = [join(TESTING_FEATURES_DIR, '{0}_all.png'.format(basename(image).split('m')[0])) for image in images]
	samples = [UnknownSample(imagepath, featurepath) for imagepath, featurepath in zip(images, features)]
	if VERBOSITY >= 2:
		print 'loaded %d samples without known classes from "%s" and "%s"' % (len(samples), PROCESSED_TESTING_IMAGES_DIR, TESTING_FEATURES_DIR)
	return samples


if __name__ == '__main__':
	from species.read_tree import get_root
	root = get_root()
	print get_testing_data()


