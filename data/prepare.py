
"""
	Python 2 code to create training images of uniform size
	saved in 'car/' (which is like a smaller train... it's a joke...)
	Also changes the 2D 0-255 values to 1D 255-0 values (used to be 1-0 but more space)
"""

from numpy import fliplr, flipud, array
from argparse import ArgumentParser
from random import sample
from sys import argv, stdout
from scipy.misc import imread, imsave
from os import listdir, makedirs
from os.path import isdir, join, splitext, basename
from features.extract import get_all_features


try:
	from run.disp import plot_data_16, show
except ImportError:
	print 'make sure you are running this from the main directory and that your PYTHONPATH starts with a : symbol'
	exit()
try:
	from numpy import pad, save, load, float32
except:
	import numpy
	print 'need numpy 1.7.0+, this PC uses', numpy.__version__
	exit()


def preprocess(test_data, dimension, align, features, mirror, skip, show_preview, seed = 1):
	""" Pre-process data for Caffe format (which is just png images). """
	""" Imports happen here so that -h shows the correct help options. """
	from settings import VERBOSITY, RAW_TRAINING_IMAGES_DIR, PROCESSED_TRAINING_IMAGES_DIR, TRAINING_FEATURES_DIR, \
		RAW_TESTING_IMAGES_DIR, PROCESSED_TESTING_IMAGES_DIR, TESTING_FEATURES_DIR
	from features.align import standardize
	from features.image import crop, make_square
	for dr in (PROCESSED_TESTING_IMAGES_DIR, TESTING_FEATURES_DIR, PROCESSED_TRAINING_IMAGES_DIR, TRAINING_FEATURES_DIR):
		try: makedirs(dr)
		except OSError: pass
	""" Collect all the image paths for either training or testing. """
	imgfns = []
	if test_data:
		raw_dir, proc_dir, feat_dir = RAW_TESTING_IMAGES_DIR, PROCESSED_TESTING_IMAGES_DIR, TESTING_FEATURES_DIR
		for fname in listdir(RAW_TESTING_IMAGES_DIR):
			imgfns.append((None, fname))
		if VERBOSITY >= 1:
			stdout.write('Testing')
	else:
		raw_dir, proc_dir, feat_dir = RAW_TRAINING_IMAGES_DIR, PROCESSED_TRAINING_IMAGES_DIR, TRAINING_FEATURES_DIR
		for dr in listdir(RAW_TRAINING_IMAGES_DIR):
			if not isdir(join(RAW_TRAINING_IMAGES_DIR, dr)):
				continue
			for fname in listdir(join(RAW_TRAINING_IMAGES_DIR, dr)):
				imgfns.append((dr, fname))
		if VERBOSITY >= 1:
			stdout.write('Training')
	""" Apply any necessary transformations to images. """
	N, last = len(imgfns), -1
	if test_data and mirror > 0:
		print 'mirroring ignored for testing data!'
		mirror = 0
	for k, (cls_dir, imgfn) in enumerate(imgfns):
		""" Load as numpy ndarray. """
		pth = join(raw_dir, cls_dir, imgfn) if cls_dir else join(raw_dir, imgfn)
		img = imread(pth)
		img = 255 - img
		to_dir = proc_dir
		if cls_dir:
			to_dir = join(proc_dir, cls_dir)
			try: makedirs(to_dir)
			except OSError: pass
		basenm = basename(splitext(imgfn)[0])
		""" Extract features """
		if features:
			feats = get_all_features(img, basenm)
			save('{0}_all.npy'.format(join(feat_dir, basenm)), feats)
			imsave('{0}_all.png'.format(join(feat_dir, basenm)), array([feats]))
		""" Square, resize and possibly align. """
		if not skip:
			if align:
				img = standardize(img, size = dimension)
			else:
				img = crop(img)
				img = make_square(img, dimension, make_square)
			assert img.shape == (dimension, dimension)
		""" Mirror images and save. """
		if not skip:
			imsave(join(to_dir, '{0}m0.png'.format(basenm)), img)
			if mirror >= 1:
				imsave(join(to_dir, '{0}m1.png'.format(basenm)), fliplr(img))
			if mirror >= 2:
				imsave(join(to_dir, '{0}m2.png'.format(basenm)), flipud(img))
			if mirror >= 3:
				imsave(join(to_dir, '{0}m3.png'.format(basenm)), flipud(fliplr(img)))
		""" Show progress """
		if VERBOSITY >= 1:
			stdout.write('.' + '.' * mirror)
			if k % 5 == 0:
				stdout.flush()
		if not 25 * k // N == last:
			last = 25 * k // N
			print '{0:3d}% {1:s}{2:s}'.format(4 * last, '*' * last, '.' * (25 - last))
	print '{0:3d}% {1:s}{2:s}'.format(100, '*' * 25, '')
	print '\nPre-processing ready!'
	if show_preview:
		print 'Showing images...'
		previews = [load(join(proc_dir, splitext(pth)[0] + '.npy')) / float32(255) for pth in sample(imgfns, 16)]
		return plot_data_16(previews)


if __name__ == '__main__':
	parser = ArgumentParser(description = 'Pre-process the data images for Caffe (doesn\'t create lmdb databases, just png images).')
	parser.add_argument('-t', '--testdata', action = 'store_true', dest = 'test_data', help = 'Convert test data instead of training data.')
	parser.add_argument('-d', '--dimension', action = 'store', dest = 'dimension', type = int, default = 100, help = 'Change the dimension of processed images (always square).')
	parser.add_argument('-a', '--align', action = 'store_true', dest = 'align', help = 'Center, scale and align all the images to make them translation, rotation and scale invariant.')
	parser.add_argument('-f', '--features', action = 'store_true', dest = 'features', help = 'Also prepare additional feature information.')
	parser.add_argument('-s', '--skip_data', action = 'store_true', dest = 'skip', help = 'Skip processing images (for now, only do features).')
	parser.add_argument('-m', '--mirror', action = 'store', default = 0, dest = 'mirror', type = int, help = 'How many mirror images to create (there are 4 including the original, so 3 is max).')
	parser.add_argument('-p', '--preview', action = 'store_true', dest = 'show_preview', help = 'Show some of the images on screen.')
	args = parser.parse_known_args(argv[1:])[0]
	from settings import SAMPLING_SEED
	preprocess(
		test_data = args.test_data,
		dimension = args.dimension,
		align = args.align,
		features = args.features,
		mirror = args.mirror,
		skip = args.skip,
		show_preview = args.show_preview,
		seed = SAMPLING_SEED
	)
	show()


