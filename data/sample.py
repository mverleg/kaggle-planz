
from numpy import float32, load, copy, array
from os.path import basename, splitext
from scipy.misc import imread
from run.cache import cache_self
from settings import ALL_DATA_IN_MEMORY, VERBOSITY


class Sample(object):
	"""
		Step probability is the probability to go from the parent node to this node.
		Total probability is the probability to go from the root to this node (which in the case of a species is supposed to be childless).
	"""

	def __init__(self, imagepath, featurepath):
		self.imagepath = imagepath
		self.featurepath = featurepath
		self.step_probability = {}

	def __repr__(self):
		return '#%s' % basename(splitext(self.imagepath)[0])

	#@property
	#def label(self):
	#	"""
	#		This assumes the name of the npy file is the same as that of the jpg except the extension.
	#	"""
	#	return '%s.jpg' % basename(splitext(self.filepath)[0])

	#@property
	#def caffeimgpath(self):
	#	return '{0}.png'.format(basename(splitext(self.filepath)[0]))

	@property
	def weak_hash(self):
		"""
			A 'probably good enough' hash, since hashing all the data is slow.
			I was thinking about str'ing every 100th element, but filepath should be okay and faster.
		"""
		return self.imagepath

	def get_image(self):
		"""
			Get the data for this sample's image as read-only float32 numpy array.

			If settings.ALL_DATA_IN_MEMORY is True, the data is cached, which may be faster, but problematic on low memory machines.

			Image pixels have only 256 values, so no need for float64.
		"""
		if ALL_DATA_IN_MEMORY:
			try:
				self._IMAGE_CACHE
			except AttributeError:
				if VERBOSITY >= 3:
					print 'image for %s loaded and cached' % self
				self._IMAGE_CACHE = imread(self.imagepath) / float32(255)
				self._IMAGE_CACHE.flags.writeable = False
			else:
				if VERBOSITY >= 3:
					print 'image for %s retrieved from cache' % self
			return copy(self._IMAGE_CACHE)
		else:
			if VERBOSITY >= 3:
				print 'image for %s loaded; cache disabled' % self
			data = imread(self.imagepath) / float32(255)
			data.flags.writeable = False
			return data

	def get_features(self):
		"""
			Like get_image, but gets an array of features.
		"""
		if ALL_DATA_IN_MEMORY:
			try:
				self._FEATURE_CACHE
			except AttributeError:
				if VERBOSITY >= 3:
					print 'features for %s loaded and cached' % self
				self._FEATURE_CACHE = imread(self.featurepath) / float32(255)
				self._FEATURE_CACHE.flags.writeable = False
			else:
				if VERBOSITY >= 3:
					print 'features for %s retrieved from cache' % self
			return copy(self._FEATURE_CACHE)
		else:
			if VERBOSITY >= 3:
				print 'features for %s loaded; cache disabled' % self
			data = imread(self.imagepath) / float32(255)
			data.flags.writeable = False
			return data

	def get_label(self):
		raise Exception('Only KnownSample objects have known labels.')

	def total_probabilities(self, options):
		#todo: possibly cache, called >= 3 times per options

		return array([self.total_probability(option) for option in options])

	def total_probability(self, option):
		"""
			Calculate the total probability from the step probabilities. Correction factors for prior probabilities
			are not yet included here.
		"""
		prob = 1.
		while not option.root:
			try:
				prob *= self.step_probability[option]
			except KeyError:
				raise self.UnknownInformation('Could not find probability for %s for sample %s; perhaps a prediction has not been made, or you used .classify instead of .classify_assign.' % (option, self))
			option = option.parent
		return prob

	def text_probabilities(self, options, compliant = False):
		"""
			Produce a string containing the probabilities for each option.

			@param compliant: set True to the submittable csv format
		"""
		probs = self.total_probabilities(options)
		txt = ''
		if compliant:
			txt += self.label
			for prob in probs:
				txt += ',%.12f' % prob
		else:
			txt += str(self)
			norm = 0
			mpopt = self.most_probable(options)[1]
			for opt, prob in zip(options, probs):
				norm += prob
				chr = ''
				if hasattr(self, 'node'):
					if opt.name == self.node.name:
						chr = '^'
				if opt.name == mpopt.name:
					chr = '*' if chr else '-'
				txt += '\t' + ('%.6f' % prob)[2:] + chr
			txt += '\t' + ('%.4f' % norm)
		return txt

	#def reset(self):
	#	raise NotImplementedError('Restoring to unpredicted state: not yet implemented.')

	def most_probable(self, options):
		"""
			@return: index, option and probability of the most likely class.
		"""
		probs = self.total_probabilities(options)
		index = probs.argmax()
		return index, options[index], probs[index]

	class UnknownInformation(Exception): pass


class KnownSample(Sample):
	"""
		This object represents one data sample with known class.
	"""

	all = set()

	def __init__(self, imagepath, featurepath, node):
		super(KnownSample, self).__init__(imagepath, featurepath)
		self.node = node
		self.masted = False
		KnownSample.all.add(self)

	#def get_goal_output(self, node):
	#	"""
	#		Get the training goal vector for this sample from a specific node.
	#
	#		I read somewhere that "In practice it has been found better to use values of 0.9 and 0.1."
	#	"""
	#	return array([TRAINING_OUTPUT_CORRECT if child.name == self.node.name else TRAINING_OUTPUT_INCORRECT for child in node.children])

	@cache_self
	def masked(self):
		"""
			Return the UnknownSample equivalent of this sample.

			The step_probability structures are references to the same.
		"""
		masked_sample = UnknownSample(self.imagepath)
		masked_sample.step_probability = self.step_probability
		return masked_sample

	def is_correct(self, options):
		return self.most_probable(options)[1].name == self.node.name

	@cache_self
	def get_label(self):
		"""
			Get a number representing the class.
		"""
		from species.output import get_output_options
		return {node.name: indx for indx, node in enumerate(get_output_options())}[self.node.name]

	#def is_correct(self):
	#	if self.local_probabilities is None:
	#		raise self.UnknownInformation('This known sample has not been classified yet, so correctness is unknown.')
	#	print self.local_probabilities.argmax(), self.index


class UnknownSample(Sample):
	"""
		This object represents one data sample without known class.
	"""


