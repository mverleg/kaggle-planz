
"""
	Split the shuffled data into test and training, using iterators to not consume too much memory, shuffling.

	Use a given seed to that you can create the same iterator twice or you can re-run an analysis.
"""

from random import getstate, setstate, seed as set_seed, shuffle
from run.disp import plot_data_16, show
from settings import SAMPLING_SEED, TRAINING_FRACTION, VALIDATION_FRACTION


def get_train_test_val(options, train_frac = TRAINING_FRACTION, val_frac = VALIDATION_FRACTION, seed = SAMPLING_SEED):
	"""
		:return: train, test, val
	"""
	#assert len(KnownSample.all), 'Load samples first using get_training_data before splitting.'
	assert 0 <= val_frac + train_frac <= 1.0001
	try:
		get_train_test_val._CACHE_DATA
	except AttributeError:
		get_train_test_val._CACHE_DATA = {}
	key = ','.join(opt.name for opt in options)
	try:
		get_train_test_val._CACHE_DATA[key]
	except KeyError:
		#todo: I don't get why this list is not always the same order
		get_train_test_val._CACHE_DATA[key] = sum([option.descendant_samples for indx, option in enumerate(options)], [])
		rand_state = getstate()
		set_seed(seed)
		shuffle(get_train_test_val._CACHE_DATA[key])
		setstate(rand_state)
	N = len(get_train_test_val._CACHE_DATA[key])
	return (
		get_train_test_val._CACHE_DATA[key][0 : int((train_frac) * N)],
		get_train_test_val._CACHE_DATA[key][int((train_frac) * N) : int((1. - val_frac) * N)],
		get_train_test_val._CACHE_DATA[key][int((1. - val_frac) * N) : N],
	)


if __name__ == '__main__':
	from species.read_tree import get_root
	root = get_root()
	train, test, validation = get_train_test_val(root.children)
	print train
	if len(train) >= 16:
		previews = [sample.get_image() for sample in train[:16]]
		plot_data_16(previews)
		show()
	else:
		print 'too few training samples to show images'


#todo: resampling
# "In general the bootstrap requires fewer model fits (often around 300) than cross-validation (10-fold cross-validation should be repeated 50-100 times for stability)." - http://stats.stackexchange.com/questions/14516/understanding-bootstrapping-for-validation-and-model-selection


