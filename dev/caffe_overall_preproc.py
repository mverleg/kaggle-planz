
"""
	In the end, pre-processing should be done by caffe_net.py, but this is useful for testing.
"""

from os.path import join
from data.split import get_train_test_val
from nn_caffe.utils import get_class_map
from settings import PROCESSED_TRAINING_IMAGES_DIR
from species.read_tree import get_root


if __name__ == '__main__':
	root = get_root()
	train, test, validate = get_train_test_val([root])
	with open(join(PROCESSED_TRAINING_IMAGES_DIR, 'train.txt'), 'w+') as fh:
		fh.write(get_class_map(train))
		print fh.name
	with open(join(PROCESSED_TRAINING_IMAGES_DIR, 'val.txt'), 'w+') as fh:
		fh.write(get_class_map(validate))
		print fh.name
	with open(join(PROCESSED_TRAINING_IMAGES_DIR, 'test.txt'), 'w+') as fh:
		fh.write(get_class_map(test))
		print fh.name


