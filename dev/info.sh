#!/usr/bin/env bash

cd train

#printf "training samples: "
#find . -iname "*.jpg" | wc -l
#printf "classes: "
#find . -mindepth 1 -type d | wc -l

find . -mindepth 1 -type d | while read categ
do
	printf "$(basename $categ)\t"
	find "$categ" -iname "*.jpg" | wc -l
done



