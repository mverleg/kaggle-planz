# -*- coding: utf-8 -*-
"""
Created on Tue Mar 24 09:30:44 2015

@author: zanjani
"""

#c:\Users\zanjani\Anaconda\python.exe c:\Users\zanjani\pylearn2\pylearn2\scripts\plot_monitor.py E:\Scientific\Education\Courses\ML_Practice\PlanktonDB\NPZ\model.pkl 
# CG,EP

import numpy as np
from pylearn2.utils import serial
from theano import function
from pylearn2.datasets.npy_npz import NpzDataset


def createPredictFunction(model):
    X = model.get_input_space().make_theano_batch()
    Y = model.fprop(X)
    return function([X], Y)

    
# mdlPth is the path to the trained pkl file.
# X is the image to be classified.
model = serial.load('E:\\Scientific\\Education\\Courses\\ML_Practice\\PlanktonDB\\NPZ\\model.pkl')
f = createPredictFunction(model)
#probability = f(X)

data = np.load('E:\\Scientific\\Education\\Courses\\ML_Practice\\PlanktonDB\\NPZ\\test_plank.npz')
Imgs = data['X']
Lbl = data['y']
probability = f(Imgs)

Score = 0
N = probability.shape[0]
for i in range(0,N):
    Score = Score + np.sum(np.multiply(Lbl[i,:],np.log(probability[i,:])))

Score = -Score/N
print "Score: " + str(Score)   

#from pylab import plot,show
#time = range(probability.shape[1])
#plot(time, probability[1,:])
#show()


