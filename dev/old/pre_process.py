
"""
	Python 2 code to create training images of uniform size
	saved in 'car/' (which is like a smaller train... it's a joke...)
	Also changes the 2D 0-255 values to 1D 255-0 values (used to be 1-0 but more space)
"""

from argparse import ArgumentParser
from random import sample, shuffle, getstate, setstate, seed as set_seed
from sys import argv, stdout
from scipy.misc import imread, imsave
from os import listdir, makedirs
from os.path import isdir, join, splitext, dirname, basename
from features.image import crop, make_square


#todo: mirror: http://docs.scipy.org/doc/numpy/reference/generated/numpy.fliplr.html#numpy.fliplr

try:
	from run.disp import plot_data_16, show
except ImportError:
	print 'make sure you are running this from the main directory and that your PYTHONPATH starts with a : symbol'
	exit()
try:
	from numpy import pad, save, load, float32
except:
	import numpy
	print 'need numpy 1.7.0+, thi   aps PC uses', numpy.__version__
	exit()


def resize_data(test_data = False, max_imgs = None, dimension = 128, cropping = False, cut_wide = False, show_preview = False, seed = 1, make_caffe = False):
	from settings import TRAINING_IMAGES_DIR, RAW_TRAINING_IMAGES_DIR, TESTING_IMAGES_DIR, RAW_TESTING_IMAGES_DIR, VERBOSITY, CAFFE_DATA_DIR
	if make_caffe:
		try:
			makedirs(CAFFE_DATA_DIR)
		except OSError: pass
	imgfns = []
	if test_data:
		raw_dir, proc_dir = RAW_TESTING_IMAGES_DIR, TESTING_IMAGES_DIR
		imgfns.extend(listdir(RAW_TESTING_IMAGES_DIR))
		if VERBOSITY >= 1: stdout.write('testing')
	else:
		raw_dir, proc_dir = RAW_TRAINING_IMAGES_DIR, TRAINING_IMAGES_DIR
		for dr in listdir(RAW_TRAINING_IMAGES_DIR):
			if not isdir(join(RAW_TRAINING_IMAGES_DIR, dr)): continue
			for fname in listdir(join(RAW_TRAINING_IMAGES_DIR, dr)):
				imgfns.append(join(dr, fname))
		if VERBOSITY >= 1: stdout.write('training')
	if max_imgs is not None:
		if max_imgs < len(imgfns):
			rand_state = getstate()
			set_seed(seed + 12)
			shuffle(imgfns)
			setstate(rand_state)
			imgfns = imgfns[:max_imgs]
	for k, imgfn in enumerate(imgfns):
		pth = join(raw_dir, imgfn)
		img = imread(pth)
		img = 255 - img
		if cropping:
			img = crop(img)
		img = make_square(img, dimension, make_square)
		assert img.shape == (dimension, dimension)
		if make_caffe:
			imsave(join(CAFFE_DATA_DIR, basename(splitext(imgfn)[0] + '.png')), img)
		# rescaling to 0 - 1 turned off because it uses more disk space
		else:
			img = img.reshape((dimension * dimension,))
			assert img.min() >= 0 and img.max() <= 255
			try:
				makedirs(join(proc_dir, dirname(imgfn)))
			except OSError: pass
			save(join(proc_dir, splitext(imgfn)[0]), img)
		if VERBOSITY >= 1:
			stdout.write('.')
			if k % 10 == 0:
				stdout.flush()
	if make_caffe:
		if VERBOSITY:
			print '\nMaking Caffe database'
	else:
		print '\nPre-processing ready!'
	if max_imgs is not None:
		print 'You have processed %d images, but note that any previous files remain in the output directory. Also note that training only works with all data present.' % max_imgs
	if show_preview:
		print 'Showing images...'
		previews = [load(join(proc_dir, splitext(pth)[0] + '.npy')) / float32(255) for pth in sample(imgfns, 16)]
		return plot_data_16(previews)


if __name__ == '__main__':
	print 'This pre-processing code is a little outdated, features since switching to Caffe haven\'t been added.'
	parser = ArgumentParser(description = 'Pre-process the data images by resizing and inverting.')
	parser.add_argument('-t', '--testdata', action = 'store_true', dest = 'test_data', help = 'Convert test data instead of training data.')
	parser.add_argument('-d', '--dimension', action = 'store', dest = 'dimension', type = int, default = 100, help = 'Change the dimension of processed images (always square).')
	parser.add_argument('-c', '--cropfirst', action = 'store_true', dest = 'cropping', help = 'Crop the image first.')
	parser.add_argument('-m', '--cutexcess', action = 'store_true', dest = 'cut_wide', help = 'Cut the large dimension instead of padding the small one.')
	parser.add_argument('-p', '--preview', action = 'store_true', dest = 'show_preview', help = 'Show some of the images on screen.')
	parser.add_argument('-a', '--align', action = 'store_true', dest = 'align', help = 'Center, scale and align all the images to make them translation, rotation and scale invariant.')
	parser.add_argument('-q', '--maxcount', action = 'store', dest = 'max_imgs', type = int, default = None, help = 'Process at most X (randomly selected) images (may cause problems for training data when training).')
	parser.add_argument('--caffe', action = 'store_true', dest = 'caffe', help = 'Convert the images to Caffe format (levelbd).')
	args = parser.parse_known_args(argv[1:])[0]
	if args.caffe:
		print 'For caffe preparation you should use prepare.py instead!'
		exit()
	from settings import SAMPLING_SEED
	resize_data(test_data = args.test_data, max_imgs = args.max_imgs, dimension = args.dimension,
		cropping = args.cropping, cut_wide = args.cut_wide, show_preview = args.show_preview,
		make_caffe = args.caffe, seed = SAMPLING_SEED
	)
	show()


