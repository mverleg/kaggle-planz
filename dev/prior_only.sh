#!/bin/bash

total_count="$(find data/train -iname "*.jpg" | wc -l)"

probs=""
while read line
do
    local_count="$(find data/train/$line -iname "*.jpg" | wc -l)"
    prob="$(echo $local_count / $total_count | bc -l | cut -c1-10)"
    probs="$probs,0$prob"
done < species/output_order.txt

#echo $probs

head results/example.csv -n 1
while read line
do
    echo "$(basename $line)$probs"
done <<< "$(find data/test/ -iname "*.jpg" -type f)"


