#!/bin/bash

# create the single_tree file automatically
cd data/train/ && find . -mindepth 1 -type d | sed -e 's/\.\/\(.*\)/\1\t-/' | sort > ../../species/tree/single_tree.tsv
cd ../../

# replace spaces with tabs in the tree file
sed -i -r -e 's/[ ]+/\t/g' $(ls species/tree/*.tsv)

# get prior probabilities (assuming they are the same for train and test data), in the correct output order
rm -f species/output_info.txt
while read line
do
    local_count="$(find data/train/$line -iname "*.jpg" | wc -l)"
    printf "%s\t%s\n" "$line" "$local_count" >> species/output_info.txt

done <<< "$(head -n 1 results/example.csv | cut -d , -f 2- | sed -e 's/,/\n/g')"


