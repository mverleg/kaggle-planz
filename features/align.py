
"""
	Image moment information.
	http://opencvpython.blogspot.nl/2012/06/contours-2-brotherhood.html
	* translation: mean-center
	* rotation: align by using image moments
	* mirror: can't think of a meaningful will to consistently get the same orientation
	* scale: original scale has meaning but this is lost, can't scale same class by same amount, so scale by intensity
	* equalize: make sure the full color space is used
"""

from numpy import array, arctan, pad, pi, zeros, sqrt, concatenate, histogram
from numpy.linalg import eigh
from os import listdir
from random import shuffle, seed
from scipy.misc import imread, imrotate, imresize
from os.path import join
from matplotlib.pyplot import subplots
from features.image import translate, crop
from run.disp import plot_data_16, show
from settings import RAW_TRAINING_IMAGES_DIR, VERBOSITY


def get_center(imgarr):
	w, h = imgarr.shape
	xshift = array([range(h)] * w)
	yshift = array([range(w)] * h).T
	tot = float(imgarr.sum())
	assert tot > 0, 'The array is zero somehow!'
	xmean = sum(sum(imgarr * xshift)) / tot
	ymean = sum(sum(imgarr * yshift)) / tot
	return xmean, ymean


def get_shift(imgarr, center = None):
	if center:
		return array(list(int(v / 2) for v in imgarr.shape)) - array(center)
	return array(list(int(v / 2) for v in imgarr.shape)) - array(get_center(imgarr))


def get_axes(arr, mean = None):
	"""
		Get the primary axes as determined by eigendecomposition of the covariance matrix.

		:return (bigger_eigenvalue, smaller_eigenvalue), (corresponding_eigenvector, idem)
	"""
	w, h = arr.shape
	if mean is None:
		mean = array([w // 2, h // 2])
	xshift = (array([range(h)] * w) - mean[0])**2
	yshift = (array([range(w)] * h).T - mean[1])**2
	xyshift = (array([range(h)] * w) - mean[0]) * (array([range(w)] * h).T - mean[1])
	C = zeros((2, 2))
	C[0, 0] = (arr * xshift).sum()
	C[1, 1] = (arr * yshift).sum()
	C[1, 0] = C[0, 1] = (arr * xyshift).sum()
	E, V = eigh(C)
	if E[0] < E[1]:
		return (E[1], E[0]), (V[:, 1], V[:, 0])
	return (E[0], E[1]), (V[:, 0], V[:, 1])


def get_orientation(imgarr, mean = None):
	"""
		Get the orientation of the image in degrees by getting the eigenvectors of the covariance matrix.
	"""
	v1 = get_axes(imgarr, mean = mean)[1][0]
	angle = arctan(v1[0] / v1[1])
	return float(angle) / pi * 180


def get_scale_factor(imgarr, size):
	"""
		Try to establish a scale factor with the purpose of:
		* Making images fit into their bounding box near-optimally.
		* Give same species (shape) specimen the same size so they overlap.

		Therefore establish a scale factor with these properties:
		* Such that scaling by this factor will give the new image a scale factor of ~ 1.
		* Independent of padding/cropping background pixels => don't use width/height, only count pixels.
		* Going from an LxL to a 2Lx2L image should double (not quadruple) the scale factor => sqrt of pixel count.
		* Shape dependent, penalty for long shapes to make them fit.
			This one is tricky: using real distance changes the L^2 scale, using scaled distance breaks background-
			independence - possibly use the width-height ratio.
		* Independent of transparency (if that is an artifact) => use treshold instead of intensity value.
	"""
	w, h = imgarr.shape
	assert w > 0 and h > 0
	S = 0
	for x in range(imgarr.shape[0]):
		for y in range(imgarr.shape[1]):
			S += imgarr[x, y] > 3
	n = get_axes(imgarr)[0]
	assym = (max(n) / min(n))**0.18
	scale_factor = sqrt(600. / S) / assym
	return scale_factor * size / 50


def crop_pad(imgarr, do_crop = True, extra_padding = None):
	if extra_padding is None:
		extra_padding = max(imgarr.shape) // 2
	assert imgarr.sum(), 'Empty image!'
	imgarr = crop(imgarr)
	w, h = imgarr.shape
	L = max(imgarr.shape)
	extra_padding = 0
	# noinspection PyTypeChecker
	imgarr = pad(imgarr, (
		(extra_padding + (L - w + 1) // 2, extra_padding + (L - w) // 2),
		(extra_padding + (L - h + 1) // 2, extra_padding + (L - h) // 2)
	), mode = 'constant', constant_values = 0)
	return imgarr


def equalize(imgarr, margin = 5, plot = False):
	"""
		Uses global histogram equalization with severeal modifications to ignore the background.

		Adapted from http://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_imgproc/py_histograms/py_histogram_equalization/py_histogram_equalization.html
	"""
	hist, bins = histogram(imgarr.flatten(), 256 - margin, [margin, 256])
	cdf = hist.cumsum()
	cdf = margin + (cdf - cdf.min()) * (255 - margin) / (cdf.max() - cdf.min())
	cdf = concatenate((array(range(margin)), cdf))
	imgarr2 = cdf[imgarr]
	if plot:
		fig, ((ax1, ax2), (ax3, ax4)) = subplots(2, 2)
		fig.tight_layout()
		ax1.plot(cdf, color = 'b')
		ax1t = ax1.twinx()
		ax1t.hist(imgarr.flatten(), 255 - margin, [margin, 256], color = 'r', edgecolor = 'none')
		ax1.set_xlim([0, 255])
		ax1.set_yticks([])
		ax1t.set_yticks([])
		hist, bins = histogram(imgarr2.flatten(), 250, [5, 256])
		cdf = hist.cumsum()
		ax2.plot(cdf, color = 'b')
		ax2t = ax2.twinx()
		ax2t.twinx().hist(imgarr2.flatten(), 255 - margin, [margin, 256], color = 'r', edgecolor = 'none')
		ax2.set_xlim([0, 255])
		ax2.set_yticks([])
		ax2t.set_yticks([])
		ax3.imshow(imgarr.T, vmin = 0, vmax = 255)
		ax3.set_xticks([])
		ax3.set_yticks([])
		ax4.imshow(imgarr2.T, vmin = 0, vmax = 255)
		ax4.set_xticks([])
		ax4.set_yticks([])
		show()
	return imgarr2


def mean_center(imgarr):
	""" Translation. """
	return translate(imgarr, get_shift(imgarr))


def align(imgarr):
	""" Rotation (assuming mean-centered data). """
	angle = get_orientation(imgarr)
	return imrotate(imgarr, - angle + 90)


def rescale(imgarr, size):
	"""
		Scale (changing dimension proportionally). If the difference is small, skip to preserve quality.
	"""
	scale = get_scale_factor(imgarr, size = size)
	if 0.9 < scale < 1.1:
		return imgarr
	return imresize(imgarr, scale)


def box_cut(imgarr, size):
	leftmargin = (imgarr.shape[0] - size + 1) // 2
	rightmargin = (imgarr.shape[0] - size) // 2
	if leftmargin > 0:
		if rightmargin == 0:
			return imgarr[leftmargin:, leftmargin:]
		return imgarr[leftmargin:-rightmargin, leftmargin:-rightmargin]
	elif rightmargin < 0:
		return pad(imgarr, ((-leftmargin, -rightmargin), (-leftmargin, -rightmargin)), mode = 'constant', constant_values = 0)
	return imgarr


def standardize(imgarr, size):
	imgarr = equalize(imgarr)
	imgarr = crop_pad(imgarr)
	imgarr = mean_center(imgarr)
	imgarr = align(imgarr)
	imgarr = rescale(imgarr, size = size)
	imgarr = box_cut(imgarr, size = size)
	assert imgarr.shape == (size, size), 'shape is %dx%d rather than %dx%d' % (imgarr.shape + (size, size))
	return imgarr


if __name__ == '__main__':
	pthli, rimgli, timgli = [], [], []
	for dr in listdir(RAW_TRAINING_IMAGES_DIR):
		dr = 'chaetognath_non_sagitta'  # remove to mix all species
		for fname in listdir(join(RAW_TRAINING_IMAGES_DIR, dr)):
			pthli.append(join(RAW_TRAINING_IMAGES_DIR, dr, fname))
		break
	seed(43)
	shuffle(pthli)
	for pth in pthli[:16]:
		if VERBOSITY >= 2:
			print pth
		imgarr = 255 - imread(str(pth))
		imgarr2 = standardize(imgarr, size = 250)
		rimgli.append(imgarr / 255.)
		timgli.append(imgarr2 / 255.)
	fig = plot_data_16(rimgli)[0]
	fig.savefig('pre.png')
	fig = plot_data_16(timgli)[0]
	fig.savefig('post.png')
	show()


