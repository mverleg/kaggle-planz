
"""
	Extract features and collect in the target directory.
"""

from numpy import array, float32, zeros, clip
from settings import EXTERNAL_FEATURES


def get_all_features(imgarr, name):
	"""
		Extract potentially useful features, either information lost during pre-processing or simply helpful features.

		Images at this point are raw - not resized, 0-255 with white background.
	"""
	#todo: turn on again (off for comparison)
	size_feats = get_size_features(255 - imgarr)
	if not EXTERNAL_FEATURES:
		size_feats[:] = 0
	return size_feats


def get_size_features(imgarr, refsize = 300.):
	sm = imgarr.sum()  # doesn't overflow this time
	cnt = (imgarr > 3).sum()
	w, h = imgarr.shape
	refsize = float32(refsize)
	features = array([
		w / refsize,
		h / refsize,
		(w + h) / (2 * refsize),
		(w * h) / refsize**2,
		(w * h)**0.5 / refsize,
		4 * sm / (255 * refsize**2),
		4 * sm**0.5 / (16 * refsize),
		4 * cnt / (refsize**2),
		4 * cnt**0.5 / refsize,
	])
	return clip(features, 0, 1)


if __name__ == '__main__':
	imgarr = zeros((300, 300), dtype = int)
	print get_size_features(imgarr)  # black
	print get_size_features(255 - imgarr)  # white
	# seems about right - values around 0-1 except for completely black/gray pictures


