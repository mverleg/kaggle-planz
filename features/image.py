
"""
	Simpler alternative to align.
"""

from numpy import roll, pad
from scipy.misc import imresize


def translate(arr, shifts, fill_value = 0):
	Ns = arr.shape
	assert len(Ns) == len(shifts), 'Provide a shift distance for each axis'
	for k, shift in enumerate(reversed(shifts)):
		shift = int(round(shift))
		if shift != 0:
			arr = roll(arr, shift = shift, axis = k)
			s = [slice(None)] * len(shifts)
			s[k] = slice(0, shift) if shift > 0 else slice(shift, Ns[k])
			arr[s] = fill_value
	return arr


def crop(arr_img):
	""" Crop padding. This assumes all values are non-negative, and the background is zero. """
	assert len(arr_img.shape) == 2
	while sum(arr_img[0, :]) == 0:
		arr_img = arr_img[1:, :]
	while sum(arr_img[-1, :]) == 0:
		arr_img = arr_img[:-1, :]
	while sum(arr_img[:, 0]) == 0:
		arr_img = arr_img[:, 1:]
	while sum(arr_img[:, -1]) == 0:
		arr_img = arr_img[:, :-1]
	return arr_img


def make_square(img, dimension, cut_wide):
	if cut_wide:
		w, h = dimension * img.shape[0] // min(img.shape), dimension * img.shape[1] // min(img.shape)
	else:
		w, h = dimension * img.shape[0] // max(img.shape), dimension * img.shape[1] // max(img.shape)
	img = imresize(img, (w, h))
	if cut_wide:
		if w > dimension:
			img = img[(w - dimension) // 2 : - (w - dimension) // 2, :]
		if h > dimension:
			img = img[:, (h - dimension) // 2 : - (h - dimension) // 2]
	else:
		img = pad(img, (
			((dimension - w + 1) // 2, (dimension - w) // 2),
			((dimension - h + 1) // 2, (dimension - h) // 2)
			), mode = 'constant', constant_values = 0)
	return img


