
"""
	Caffe data can be fed from memory, but I don't want to re-make the whole thing later if memory becomes too small,
	so let's use file database everywhere.
"""

from dircache import listdir
from genericpath import isfile
from numpy import copy, array
from os.path import join, exists, dirname
from random import shuffle
from scipy.misc import imread
from subprocess import Popen, PIPE, STDOUT
from caffe import Net, TEST
from os import mkdir
from classifiers.classifier import BaseClassifier
from data.split import get_train_test_val
from nn_caffe.utils import make_node_lmdb
from settings import VERBOSITY, CLASSIFIER_STATE_DIR, CAFFE_CODE_DIR


class CaffeClassifier(BaseClassifier):
	"""
		Neural network classifier.
	"""

	defaults = {
		'solver':  'nn_caffe/nets/template_solver.prototxt',
		'network': 'nn_caffe/nets/template_network_lmdb_v2.prototxt',  # note that network is the template and net is the parsed version
		'test_iter': 500,
		'test_interval': 100,
		'base_lr': 0.00001,
		'gamma': 0.1,
		'stepsize': 1000,
		'max_iter': 1500,
		'momentum': 0.9,
		'weight_decay': 0.005,
		'snapshot': 3000,
		'mirror': 'false',
		'crop': None,
		'batch_size': 128,
		'inner_product_size': 400,  # 512
	}

	def __init__(self, node, options = None, *args, **kwargs):
		super(CaffeClassifier, self).__init__(node, *args, **kwargs)
		""" Some default params that are specific to the instance or the data. """
		self.state_dir = join(CLASSIFIER_STATE_DIR, '{0}'.format(self.node.name))
		self.network_path, self.solver_path = join(self.state_dir, 'network.prototxt'), join(self.state_dir, 'solver.prototxt')
		self.params['net'] = self.network_path
		self.params['crop_txt'] = 'crop: {0d}'.format(self.params['crop']) if self.params['crop'] else '#no cropping'
		example = get_train_test_val(options)[0][0]
		self.params['dimension'] = imread(example.imagepath).shape[0]  # todo: dynamic
		self.params['feature_count'] = max(imread(example.featurepath).shape)  # todo: dynamic
		self.params['class_count'] = len(options)  # todo: dynamic
		self.params['image_train_path'] = join(self.state_dir, 'image_train')
		self.params['feature_train_path'] = join(self.state_dir, 'feature_train')
		self.params['image_test_path'] = join(self.state_dir, 'image_test')
		self.params['feature_test_path'] = join(self.state_dir, 'feature_test')
		assert isfile(self.solver), "Caffe solver file \"{0}\" not found".format(self.solver)
		assert isfile(self.network), "Caffe network file \"{0}\" not found".format(self.network)
		""" Create network and solver files from templates. """
		try:
			mkdir(self.state_dir)
		except OSError:
			pass
		with open(self.network, 'r') as fh:
			net_txt = fh.read().format(**self.params)
		with open(self.network_path, 'w+') as fh:
			fh.write(net_txt)
		self.params['snapshot_prefix'] = join(self.state_dir, 'snapshot')
		with open(self.solver, 'r') as fh:
			solver_txt = fh.read().format(**self.params)
		with open(self.solver_path, 'w+') as fh:
			fh.write(solver_txt)
		if VERBOSITY >= 2:
			if exists(self.solver):
				print 'existing network files for "%s" have been overwritten' % self.node
			else:
				print 'new network files for "%s" have been created' % self.node

	@property
	def latest_snapshot(self):
		dir, prefix = self.params['snapshot_prefix'].rsplit('/', 1)
		snapshots = [join(dir, fname) for fname in listdir(dir) if fname.startswith(prefix) and fname.endswith('.caffemodel')]
		return max(snapshots, key = lambda snap: int(snap.split('iter_')[-1].split('.')[0]))

	def load(self):
		"""
			Load the current training state. Continue if no save file is found.
		"""
		#todo: maybe select the last snapshot, use for further training or prediction
		if VERBOSITY >= 3:
			print 'load has not effect for {0} ({1})'.format(self.node, self.__class__.__name__, self.node)

	def save(self):
		"""
			Save the current training state.

			Make sure that params that invalidate the state are part of the filename.
			E.g. the learning rate can change between runs, but the neural network size can not.
		"""
		if VERBOSITY >= 3:
			print 'save has not effect for {0} ({1})'.format(self.node, self.__class__.__name__, self.node)

	def make_lmdbs(self):
		"""
			Create lmdb files since MemoryData layer doesn't work with pycaffe:
				https://github.com/BVLC/caffe/issues/2246
		"""
		""" Get the options that this node can choose between. """
		train, test, validate = get_train_test_val(self.options)
		""" Make sure to shuffle samples before image/feature extraction, as these should be aligned.
			EDIT: Shuffle is redundant (already in get_train_test_val) but kept to keep old code working. """
		shuffle(train, random = lambda: 0.5)
		shuffle(validate, random = lambda: 0.5)
		""" Making th lmdb databases. """
		print 'if filenames appear within 2s it probably failed unless you have few samples'
		image_train = make_node_lmdb(samples = train, node = self.node, basepath = self.params['image_train_path'])
		feature_train = make_node_lmdb(samples = train, node = self.node, basepath = self.params['feature_train_path'], features = True)
		image_test = make_node_lmdb(samples = validate, node = self.node, basepath = self.params['image_test_path'])
		feature_test = make_node_lmdb(samples = validate, node = self.node, basepath = self.params['feature_test_path'], features = True)
		print 'Created "{0:s}", "{1:s}", "{2:s}" and "{3:s}".'.format(image_train, feature_train, image_test, feature_test)
		return train, validate

	def train(self, samples = 'ignored'):
		"""
			Train this classifier with a given data set for each option.

			See BaseClassifier.
		"""

		print 'Making database files'
		self.make_lmdbs()

		print 'Starting network training for %s' % self
		logfile = join(self.state_dir, 'train.log')
		cmd = ' '.join([join(CAFFE_CODE_DIR, 'build/tools/caffe'), 'train',
			'-solver', self.solver_path, '2>&1 | tee "{0:s}"'.format(logfile)])
		if VERBOSITY >= 1:
			print cmd
		proc = Popen(cmd, stdout = PIPE, stderr = STDOUT, shell = True)
		out = proc.communicate()[0]
		print out
		print 'Network training for {0:s} terminated, output written to "{1:s}"'.format(self, logfile)

		return

	def classify(self, samples = 'ignored'):
		"""
			Given samples without class specified, assign a probability for each class.

			:return: probabilities, samples, classes

			See BaseClassifier.
		"""
		print 'Creating network "{0:s}" from state {1:s}"'.format(self.network_path, self.latest_snapshot)
		net = Net(self.network_path, self.latest_snapshot, TEST)
		print 'Layers: {0:s}'.format(', '.join(net.blobs.keys()))

		""" Get the options that this node can choose between. """
		with open('{0:s}.txt'.format(self.params['image_test_path']), 'r+') as fh:
			image_paths = [line.split()[0] for line in fh.read().splitlines()]
		validate = get_train_test_val(self.options)[-1]
		shuffle(validate, random = lambda: 0.5)  # for alignment with make_lmdbs
		for smpl, impth in zip(validate, image_paths):
			assert smpl.imagepath == impth, 'Validation data not aligned anymore (changed seed?):\n  %s /= %s' % (smpl.imagepath, impth)

		print "Predicting classes"
		probabilities, last, N = [], -1, len(validate)
		for k in range(N):
			result = net.forward()
			probabilities.append(copy(result['prob'][0]))
			if not 25 * k // N == last:
				last = 25 * k // N
				print '{0:3d}% {1:s}{2:s}'.format(4 * last, '*' * last, '.' * (25 - last))
			#if k > 40:
			#	break
		print '{0:3d}% {1:s}{2:s}'.format(100, '*' * 25, '')
		probabilities = array(probabilities)
		classes = copy(self.options)
		return probabilities, validate, classes


