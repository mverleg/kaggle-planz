
from multiprocessing import Pool
from numpy import array, ones, zeros
from sys import argv
from nn_caffe.utils import get_map_raw
from species.output import get_output_options
from species.read_tree import get_root, breadth_first
from data.split import get_train_test_val


def _train_node(node):
	if node.classifier is not None:
		if len(node.classifier.options) > 1:
			root = get_root()
			train, test, validate = get_train_test_val([root])
			print 'training {0}'.format(node)
			node.classifier.train(samples = train)
			node.classifier.save()


def train_nets():
	"""
		Train the CaffeClassifier (for simple single node).
	"""
	from settings import PROCESS_COUNT
	root = get_root()
	#_handle_node(list(breadth_first(root))[2])
	print Pool(PROCESS_COUNT).map(_train_node, list(breadth_first(root)))


def test_nets():
	"""
		Predict validation data with all the species nodes.
	"""
	""" Collect all the probabilities in a matrix. """
	root = get_root()
	allclasses = {val: key for key, val in enumerate(list(breadth_first(root)))}
	rawprobs = [None for cls in allclasses]
	samples = None
	for node in allclasses.keys():
		if node.classifier:
			if len(node.classifier.options) > 1:
				print 'Classifying {0:s}'.format(node)
				localprobs, samples, localclasses = node.classifier.classify()
				for k, cls in enumerate(localclasses):
					rawprobs[allclasses[cls]] = localprobs[:, k]
	rawprobs[0] = zeros(rawprobs[1].shape)
	rawprobs = array(rawprobs).T
	totalprobs = ones(rawprobs.shape)
	""" Propagate the probabilities down the tree - should happen top -> bottom! """
	for cls, indx in allclasses.items():
		if cls.root:
			continue
		parentindx = allclasses[cls.parent]
		totalprobs[:, indx] = rawprobs[:, indx] * totalprobs[:, parentindx]
	totalprobs[:, 0] = 0
	""" Rescale with priors """
	#todo
	""" Get the accuracy. """
	clsmap = {node.name: indx for indx, node in enumerate(get_output_options())}
	predictions = totalprobs.argmax(1)
	trueclss = [clsmap[sample.node.name] for sample in samples]
	accuracy = (trueclss == predictions).mean()
	print 'Classes: {0:d}  samples: {1:d}'.format(len(clsmap), len(samples))
	print 'Accuracy: {0:8.4f}%  (random is {1:.1f}%)'.format(100 * accuracy, 100. / len(clsmap))


if __name__ == '__main__':
	if not len(argv) >= 2:
		print 'type "train" or "test"'
		exit()
	if argv[1] == 'train':
		train_nets()
	elif argv[1] == 'test':
		test_nets()
	else:
		print 'unknown "%s", type "train" or "test"' % argv[1]


