from numpy.matlib import randn

__author__ = 'tim'

import numpy as np
import matplotlib.pyplot as plt
import os
import caffe

# Make sure that caffe is on the python path:
root = './'  # this file is expected to be in {repository_root}

import sys

sys.path.insert(0, root + 'caffe/python')

plt.rcParams['figure.figsize'] = (5, 5)
plt.rcParams['image.interpolation'] = 'nearest'
plt.rcParams['image.cmap'] = 'gray'


if __name__ == '__main__':
    caffe.set_mode_cpu()
    net = caffe.Net(root + 'nn_caffe/network_tim.prototxt', root + 'snaps/tim/_iter_2000.caffemodel', caffe.TEST)

    for k, v in net.params.items():
        if "conv" in k:
            filters = net.params[k][0].data
            n = 4
            fig,axi = plt.subplots(n,n)
            fig.tight_layout()
            for i,x in enumerate(axi):
                for j,y in enumerate(x):
                    if(i*(n-1)+j < filters.size):
                        y.imshow(filters[i*(n-1)+j][0])
                        y.set_xticks([])
                        y.set_yticks([])
            plt.show()



