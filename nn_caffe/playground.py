
"""
	Just some trial code getting Caffe to work.
	-Mark

	first run (to get the data)
	caffe/scripts/download_model_binary.py caffe/models/bvlc_reference_caffenet
	caffe/data/ilsvrc12/get_ilsvrc_aux.sh
"""

# the ._caffe shouldn't be needed but PyCharm likes it
from multiprocessing import Pool
from numpy import load, asarray, copy, tile, concatenate
from caffe._caffe import Net
from caffe import Classifier, TRAIN
from matplotlib.pyplot import show, subplots
from caffe.io import load_image, resize_image
from species.read_tree import get_root, breadth_first
from data.split import get_train_test_val
from species.tree_node import TreeNode


def classify_demo():
	# mostly based on these steps:
	# http://nbviewer.ipython.org/github/BVLC/caffe/blob/master/examples/hdf5_classification.ipynb
	# some information about custom data:
	# https://github.com/BVLC/caffe/issues/747

	MODEL_FILE = 'nn_caffe/nets/demo.prototxt'
	PRETRAINED = 'caffe/models/bvlc_reference_caffenet/bvlc_reference_caffenet.caffemodel'
	IMAGE_FILE_01 = 'nn_caffe/nets/test_img_01.jpg'
	IMAGE_FILE_02 = 'nn_caffe/nets/test_img_02.jpg'
	CLASS_FILE = 'caffe/data/ilsvrc12/synset_words.txt'

	mean = load('caffe/python/caffe/imagenet/ilsvrc_2012_mean.npy').mean(1).mean(1)
	print mean

	net = Classifier(MODEL_FILE, PRETRAINED,
		mean = mean, # todo: off (remove)
		channel_swap = (2,1,0), # todo: no need for 1 color data
		raw_scale = 255,
		image_dims = (256, 256),
	)

	# note that data is simply a numpy ndarray, just like the data we use so far
	img1 = load_image(IMAGE_FILE_01)
	img2 = load_image(IMAGE_FILE_02)
	print 'caffe data', type(img1), img1.shape

	# automated, with resizing etc included
	prediction = net.predict([img1, img2], oversample = False)  # for fair comparison, no oversampling

	# manual, so you can choose your own pre-processing (also pre-process in advance)
	#inp = reshape(resize_image(img1, net.image_dims), (1,) + net.image_dims + (3,))  # I can't change batch size

	def prep(img):
		# resize
		inp = resize_image(img, net.image_dims)
		# take center crop
		center = asarray(net.image_dims) / 2.0
		crop = tile(center, (1, 2))[0] + concatenate([
			-net.crop_dims / 2.0,
			net.crop_dims / 2.0
		])
		inp = inp[crop[0]:crop[2], crop[1]:crop[3], :]
		# special pre-processing
		return asarray([net.transformer.preprocess('data', inp)])

	cinp1 = prep(img1)
	cinp2 = prep(img2)
	pprediction = asarray([
		copy(net.forward(data = cinp1)['prob'][0]),
		copy(net.forward(data = cinp2)['prob'][0]),
	])  # do not forget copy! the array is reused for new computations!

	# results
	print 'prediction shape:', prediction.shape
	cls1, cls2 = prediction[0].argmax(), prediction[1].argmax()
	pcls1, pcls2 = pprediction[0].argmax(), pprediction[1].argmax()
	classes = open(CLASS_FILE, 'r').readlines()
	print 'predicted class 1:', cls1, classes[cls1].strip()
	print 'predicted class 2:', cls2, classes[cls2].strip()
	print 'other likely options for img 2:'
	for cls, probability in zip(classes, prediction[1]):
		if probability > 0.02:
			print '%.4f %s' % (probability, cls.strip())
	print 'so elephant is the second most likely option - and it got the right kind (African)'

	# figures
	fig, ((ax1, ax2), (ax3, ax4)) = subplots(2, 2, figsize = (14, 10))
	ax1.imshow(resize_image(img1, net.image_dims))
	ax2.imshow(resize_image(img2, net.image_dims))
	ax3.plot(prediction[0], color = 'blue')
	ax3.plot(pprediction[0], color = 'red')
	ax4.plot(prediction[1], color = 'blue')
	ax4.plot(pprediction[1], color = 'red')


def test_caffe_net():
	"""
		Test the CaffeClassifier (for simple single node).
	"""
	root = get_root()
	train, test, validate = get_train_test_val([root])
	node = list(breadth_first(root))[0]
	print node
	#print node.classifier.classify_assign(samples = [item.masked() for item in validate])
	if node.classifier is not None:
		if len(node.classifier.options) > 1:
			node.classifier.train(samples = train)
			node.classifier.save()


def predict_data(root, samples, load = True):
	# todo parallelism
	# pool = Pool(processes = PROCESS_COUNT)
	# mp = partial(pool.map, chunksize = 1) if parallel else map

	"""
		Classify results.
	"""
	for node in breadth_first(root):
		if node.classifier is not None:
			if load: node.classifier.load()
			for sample in samples:
				node.classifier.classify_assign(sample = sample.masked())

	"""
		Show the results.
	"""
	class_prob = 0
	total_correct = 0
	out_options = get_output_options()
	if VERBOSITY >= 1:
		print '\t' + '\t'.join(str(opt)[-6:] for opt in out_options).upper()
	for sample in samples:
		class_prob += sample.total_probabilities(out_options)
		if sample.is_correct(out_options): total_correct += 1
		if VERBOSITY >= 1:
			print sample.text_probabilities(out_options, compliant = False)
	print '\nclass probabilities (classified and prior)'
	print 'PREDICT\t' + '\t'.join(('%.6f' % prob)[2:] for prob in class_prob / len(samples))
	print 'PRIOR\t' + '\t'.join(('%.6f' % prob)[2:] for prob in get_output_probabilities())
	print 'total correct: %d = %.1f%%  (randomly expects %.1f correct)' % (total_correct, 100 * float(total_correct) / len(samples), len(samples) / len(out_options))


def train_on_data(root, samples):
	for node in breadth_first(root):
		if node.classifier is not None:
			if len(node.classifier.options) > 1:
				if VERBOSITY >= 1:
					print 'training', node
				node.classifier.train(samples = samples)
				node.classifier.save()


	MODEL_FILE = 'nn_caffe/nets/network1.prototxt'
	net = Net(MODEL_FILE, 'demo.caffemodel', TRAIN)
	#todo: See https://www.kaggle.com/c/datasciencebowl/forums/t/12765/cnn-caffe/66083
	#todo: might need this https://www.kaggle.com/c/datasciencebowl/forums/t/12765/cnn-caffe/65973#post65973


def just_play():
	root = get_root()
	# optionally, separates into train, test, validation
	train, test, validate = get_train_test_val(root.classifier.options) # optional
	print TreeNode.nodes.keys()


if __name__ == '__main__':
	#classify_demo()
	#train_own()
	#just_play()
	#train_caffe_net()
	#test_caffe_net()
	print 'enable something'
	show()


