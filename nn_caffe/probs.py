#!/usr/bin/python

"""
	Seems the signature is different for data layer and data definition outside layer, sorry if I broke the other one.

	Also adapted it to the aligned data and removed the need for an image file.

	https://github.com/BVLC/caffe/issues/1774
	https://groups.google.com/forum/#!topic/caffe-users/AB_6GZaO3ok

	In the end I'm using the lmdb file (see train_steps.sh to make it), so all the image loading is just here in case
	pycaffe at some points starts working correctly. Or if a network without data layers is trained.
"""

from numpy import array, swapaxes, copy, save
from os.path import join
from caffe import Net, TEST
from scipy.misc import imread
from argparse import ArgumentParser
from sys import argv


def get_probabilities(model_file, pretrained_file, limit = None):

	print 'Creating network "{0:s}" from state {1:s}"'.format(args.model_file, args.pretrained_file)
	net = Net(args.model_file, args.pretrained_file, TEST)
	print 'Layers: {0:s}'.format(', '.join(net.blobs.keys()))

	if True:  # disabled while using tmdb
		img_paths = get_sample_list()
		if limit:
			img_paths = img_paths[:limit]
		print 'Loading {0:d} images (may take a minute)'.format(len(img_paths))
		input_images = []
		for pth in img_paths:
			if pth.endswith('.png'):
				input_image = array([imread(pth)])
				input_image = input_image.reshape((70, 70, 1))
				input_images.append(input_image)
		input_images = array(input_images)
		""" The expected format is H x W x K so swap. """
		input_images = swapaxes(input_images, 1, 3)
		print 'Memory use: {0:d}MB'.format(input_images.nbytes / 1024**2)
	else:
		print 'Using LMDB, no images loaded'
		input_images = get_sample_list()
		if limit:
			input_images = input_images[:limit]

	print "Predicting classes"
	probabilities = []
	N, last = len(input_images), -1
	for k, input_image in enumerate(input_images):
		""" result = net.forward() """
		result = net.forward(data = array([input_image]))
		probabilities.append(copy(result['prob'][0]))
		if not 25 * k // N == last:
			last = 25 * k // N
			print '{0:3d}% {1:s}{2:s}'.format(4 * last, '*' * last, '.' * (25 - last))
	print '{0:3d}% {1:s}{2:s}'.format(100, '*' * 25, '')
	probabilities = array(probabilities)
	return probabilities


def get_sample_list():
	"""
		Uses the sample order from the test dir, which should be the order of example.csv output file.
	"""
	from settings import PROCESSED_TESTING_IMAGES_DIR
	with open(join(PROCESSED_TESTING_IMAGES_DIR, 'forprobs.txt'), 'r+') as fh:
		return [line.rstrip('\t0') for line in fh.read().splitlines()]


if __name__ == '__main__':
	parser = ArgumentParser(description = 'Calculates probabilities of a test object with trained caffemodel file')
	parser.add_argument('-m', '--model', action = 'store', dest = 'model_file', help = 'Caffe DEPLOY version of model prototxt file.')
	parser.add_argument('-p', '--pretrained', action = 'store', dest = 'pretrained_file', help = 'Caffemodel pretrained file (.caffemodel).')
	parser.add_argument('-i', '--input', action = 'store', dest = 'input_file', help = '[DEPRECATED] Input file of the to be classified image filenames. Images should be in data/test/')
	parser.add_argument('-c', '--classes', action = 'store', dest = 'class_file', help = 'Classes file.')
	parser.add_argument('--limit', action = 'store', dest = 'limit', type = int, help = 'Use subset of data (for debugging)')
	args = parser.parse_known_args(argv[1:])[0]
	from settings import PROCESSED_TESTING_IMAGES_DIR
	assert not args.input_file, 'input_file argument has been removed'
	if not args.model_file or not args.pretrained_file:
		parser.print_help()
		exit()
	probs = get_probabilities(model_file = args.model_file, pretrained_file = args.pretrained_file,	limit = args.limit)
	if not args.limit:
		save(join(PROCESSED_TESTING_IMAGES_DIR, 'probabilities.npy'), probs)
	else:
		print 'saving skipped due to limit'


