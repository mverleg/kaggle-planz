#!/bin/bash

# rescale etc images (takes some time)
# python data/pre_process.py --cropfirst --preview --caffe --dimension 70 -v
# python data/prepare.py --dimension 70 --align --mirror 3 --preview -v

# create the data structure needed by Caffe
python dev/caffe_overall_preproc.py
\rm -rf data/bean/train.lmdb ; GLOG_logtostderr=1 caffe/build/tools/convert_imageset -gray -shuffle ./ data/bean/train.txt data/bean/train.lmdb
\rm -rf data/bean/val.lmdb   ; GLOG_logtostderr=1 caffe/build/tools/convert_imageset -gray -shuffle ./ data/bean/val.txt data/bean/val.lmdb

# for training database (no shuffle!):
\cat results/example.csv | tail -n+2 | cut -d ',' -f1 | sed "s/\.jpg/m0\.png/" | xargs printf 'data/slo/%s\t0\n' > data/slo/forprobs.txt
\rm -rf data/slo/forprobs.lmdb ; GLOG_logtostderr=1 caffe/build/tools/convert_imageset -gray ./ data/slo/forprobs.txt data/slo/forprobs.lmdb

# compute mean (for network3)
#   it is peculiar that the script is in .build_release which is hidden, it should be in build/tools (but isn't)
# caffe/.build_release/tools/compute_image_mean data/bean/train.lmdb data/bean/mean.binaryproto

# start training
#   you'll need to change some paths and perhaps crop_size (size of shifting window)
caffe/build/tools/caffe train -solver=nn_caffe/nets/solver3_v4.prototxt


# to resume from the (badly) convered network, use without ##:
##caffe/build/tools/caffe train -solver=/home/mark/study/patrec1/nn_caffe/nets/solver3.prototxt --snapshot=nn_caffe/nets/snapshot_converged.solverstate


