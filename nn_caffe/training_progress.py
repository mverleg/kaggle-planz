
from argparse import ArgumentParser
from subprocess import Popen, PIPE
from matplotlib.pyplot import subplots, show
from matplotlib import rcParams
rcParams.update({
	'figure.autolayout': True,
	'legend.labelspacing': 0.,
	'legend.handletextpad': 0.,
})


parser = ArgumentParser(description = 'makes a plot from Caffe output')
parser.add_argument('output_file', help = 'file of captured stdout and stderr')
args = parser.parse_args()
fn = args.output_file

def bash(cmd):
	print cmd
	p = Popen(cmd, shell = True, stdout = PIPE)
	return p.communicate()[0].splitlines()

try: train_iters = [int(q) for q in bash("cat '{0:s}' | grep 'Iteration' | grep 'lr' | cut -d ' ' -f 7 | cut -d ',' -f 1".format(fn))]
except ValueError: train_iters = [int(q) for q in bash("cat '{0:s}' | grep 'Iteration' | grep 'lr' | cut -d ' ' -f 6 | cut -d ',' -f 1".format(fn))]
train_accus = [100 * float(q) for q in bash("cat '{0:s}' | grep 'Train' | grep 'accuracy' | tr -s ' ' | cut -d ' ' -f 11".format(fn))]
train_loss = [float(q) for q in bash("cat '{0:s}' | grep 'Train' | grep 'loss' | tr -s ' ' | cut -d ' ' -f 11".format(fn))]
try: test_iters = [int(q) for q in bash("cat '{0:s}' | grep 'Testing net' | cut -d ' ' -f 7 | cut -d ',' -f 1".format(fn))]
except: test_iters = [int(q) for q in bash("cat '{0:s}' | grep 'Testing net' | cut -d ' ' -f 6 | cut -d ',' -f 1".format(fn))]
test_accus = [100 * float(q) for q in bash("cat '{0:s}' | grep 'Test' | grep 'accuracy' | tr -s ' ' | cut -d ' ' -f 11".format(fn))]

print 'train_iters', len(train_iters)
print 'train_accus', len(train_accus)
print 'train_loss', len(train_loss)
print 'test_iters', len(test_iters)
print 'test_accus', len(test_accus)

fig, axacc = subplots(figsize = (4, 2.5))
axloss = axacc.twinx()
l1 = axacc.plot(train_iters, train_accus, label = 'train acc.', color = 'blue')
l2 = axacc.plot(test_iters, test_accus, label = 'test acc.', color = 'red')
l3 = axloss.plot(train_iters, train_loss, label = 'train loss', color = 'green')
axloss.set_ylabel('Loss (multiclass log)')
axacc.set_xlabel('Iteration (batches)')
axacc.set_ylabel('Accuracy (%)')
axacc.set_xticks([])
axacc.set_ylim([0, 70])
labels = [l.get_label() for l in (l1 + l2 + l3)]
axloss.legend(l1 + l2 + l3, labels, loc = 'lower center')


if __name__ == '__main__':
	show()


