
"""
	Documentation for the Python interface is unusable so here are some wrappers for command line things.
"""

from os.path import join, exists
from subprocess import Popen, PIPE
from shutil import rmtree
from settings import VERBOSITY
from species.output import get_output_options


class CmdError(Exception): pass


def run_or_fail(cmdli, err_ok = True):
	"""
		:param cmdli: list of the command components (Popen format)
		:param err_ok: if True, stderr is accepted as non-problematic (Caffe uses stderr all over the place)
	"""
	if VERBOSITY >= 1:
		print ' '.join(cmdli)
	proc = Popen(cmdli, stdout = PIPE, stderr = PIPE)
	out, err = proc.communicate()
	if err and not err_ok:
		raise CmdError(err)
	if proc.returncode:
		raise CmdError('{0}\n{1}\n{2}'.format(' '.join(cmdli), out, err))
	if VERBOSITY >= 2:
		print '{0}\n{1}\n{2}'.format(' '.join(cmdli), out, err)
	return out


def make_lmdb(samples, basepath, features = False):
	"""
		Make an lmdb database, overwriting the old one if it exists.

		:param basepath: should be the path without extension; .lmdb will be appended for database, .txt for class list
		:return: path to the lmdb file
	"""
	#todo: remove if unused
	from settings import CAFFE_CODE_DIR
	clspath, lmdbpath = '{0}.txt'.format(basepath), '{0}.lmdb'.format(basepath)
	if exists(lmdbpath):
		rmtree(lmdbpath)
	bin = join(CAFFE_CODE_DIR, 'build/tools/convert_imageset')
	with open(clspath, 'w+') as fh:
		fh.write(get_class_map(samples, features = features))
	run_or_fail([bin, '-gray', './', clspath, lmdbpath])
	return lmdbpath


def make_node_lmdb(samples, basepath, node, features = False):
	"""
		Like make_lmdb, but with class label based on node direct descendants.
	"""
	#todo: remove if unused
	from settings import CAFFE_CODE_DIR
	clspath, lmdbpath = '{0}.txt'.format(basepath), '{0}.lmdb'.format(basepath)
	if exists(lmdbpath):
		rmtree(lmdbpath)
	bin = join(CAFFE_CODE_DIR, 'build/tools/convert_imageset')
	with open(clspath, 'w+') as fh:
		fh.write(get_class_map_node(samples, node, features = features))
	run_or_fail([bin, '-gray', './', clspath, lmdbpath])
	return lmdbpath


def get_class_map(samples, features = False):
	"""
		Convert a list of training samples to Caffe format.
		https://github.com/BVLC/caffe/issues/747
	"""
	out_map = {node.name: indx for indx, node in enumerate(get_output_options())}
	clsmap = ''
	for sample in samples:
		clsmap += '%s\t%s\n' % (sample.featurepath if features else sample.imagepath, out_map[sample.node.name])
	return clsmap.rstrip('\n')


def get_map_raw(samples, node):
	options = {node.name: indx for indx, node in enumerate(node.classifier.options)}
	out_map = {}
	descs = list(options.keys())
	for indx, orig in enumerate(get_output_options()):
		node = orig
		while not node.name in descs:
			if node.root:
				break
			node = node.parent
		if node.root:
			continue
		out_map[orig.name] = options[node.name]
	return out_map


def get_class_map_node(samples, node, features = False):
	"""
		Convert a list of training samples to Caffe format.
		https://github.com/BVLC/caffe/issues/747
	"""
	out_map = get_map_raw(samples, node)
	clsmap = ''
	for sample in samples:
		clsmap += '%s\t%s\n' % (sample.featurepath if features else sample.imagepath, out_map[sample.node.name])
	return clsmap.rstrip('\n')

