
Caffe:
http://caffe.berkeleyvision.org/gathered/examples/imagenet.html
http://nbviewer.ipython.org/github/BVLC/caffe/blob/master/examples/classification.ipynb
https://github.com/BVLC/caffe/issues/747
Info on layers: http://caffe.berkeleyvision.org/tutorial/layers.html
Training: http://caffe.berkeleyvision.org/gathered/examples/mnist.html
Thumb rules: http://caffe.berkeleyvision.org/tutorial/solver.html
Using Python: http://nbviewer.ipython.org/github/udibr/caffe/blob/master/examples/classification.ipynb
Double data layers: https://github.com/BVLC/caffe/issues/1381


compare:
  - baseline
  - many InnerProduct (original)
  - with features
  - with species
special classes (unknowns) not treated


git fame --exclude=caffe/,results/
