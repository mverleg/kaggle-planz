

def cache_noarg(view_func):
	"""
		Cache the output of a function regardless of arguments. (So if you call f(4) and then f(7), f(7) will return f(4)'s cached values).
		[tested]
	"""
	def func_with_cache(*args, **kwargs):
		try:
			view_func.CACHE
		except AttributeError:
			view_func.CACHE = view_func(*args, **kwargs)
		return view_func.CACHE
	return func_with_cache


def cache_self(view_func):
	"""
		Cache the output of a function, considering only self argument.
		[tested]
	"""
	def func_with_cache(self, *args, **kwargs):
		try:
			view_func.CACHE
		except AttributeError:
			view_func.CACHE = {}
		try:
			view_func.CACHE[self]
		except KeyError:
			view_func.CACHE[self] = view_func(self, *args, **kwargs)
		return view_func.CACHE[self]
	return func_with_cache


