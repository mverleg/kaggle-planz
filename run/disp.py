
from matplotlib.pyplot import subplots, show, cm, rcParams
# show is imported by other modules, don't remove


def plot_data(ax, dat):
	N = int((dat.shape[0])**.5)
	#rcParams['toolbar'] = 'None'
	ax.imshow(1 - dat, interpolation = 'nearest', cmap = cm.Greys_r, vmin = 0, vmax = 1)
	ax.set_xticks([])
	ax.set_yticks([])


def plot_data_window(dat):
	fig, ax = subplots(figsize = (4, 4))
	plot_data(ax, dat)


def plot_data_16(dats):
	assert len(dats) == 16, 'need at least 16 images to preview'
	fig, axi = subplots(4, 4, figsize = (10, 10))
	fig.tight_layout()
	for k in range(16):
		plot_data(axi[k % 4][k // 4], dats[k])
	return fig, axi


