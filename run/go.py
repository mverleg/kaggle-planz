
from os import listdir, path
from data.split import get_train_test_val
from settings import VERBOSITY, PROCESSED_TRAINING_IMAGES_DIR
from species.output import get_output_options, get_priors
from species.read_tree import get_root, breadth_first

#todo: some way to recover after crash (train nodes independently)

"""
	Load the species tree and initialize the choosers.
"""

def ready_go():
	print 'This may be outdated now'
	assert path.isdir(PROCESSED_TRAINING_IMAGES_DIR), 'The training data directory does not exist; did you run the pre-processing script?'
	assert len(listdir(PROCESSED_TRAINING_IMAGES_DIR)), 'The training data directory is empty; did you run the pre-processing script?'
	root = get_root()
	train, test, validate = get_train_test_val([root])
	predict_data(root, test, load = False)
	train_on_data(root, train)
	predict_data(root, test)


def predict_data(root, samples, load = True):
	# todo parallelism
	# pool = Pool(processes = PROCESS_COUNT)
	# mp = partial(pool.map, chunksize = 1) if parallel else map

	"""
		Classify results.
	"""
	for node in breadth_first(root):
		if node.classifier is not None:
			if load:
				node.classifier.load()
			node.classifier.classify_assign(samples = [sample.masked() for sample in samples])

	"""
		Show the results.
	"""
	class_prob = 0
	total_correct = 0
	out_options = get_output_options()
	if VERBOSITY >= 1:
		print '\t' + '\t'.join(str(opt)[-6:] for opt in out_options).upper()
	for sample in samples:
		class_prob += sample.total_probabilities(out_options)
		if sample.is_correct(out_options): total_correct += 1
		if VERBOSITY >= 1:
			print sample.text_probabilities(out_options, compliant = False)
	print '\nclass probabilities (classified and prior)'
	print 'PREDICT\t' + '\t'.join(('%.6f' % prob)[2:] for prob in class_prob / len(samples))
	print 'PRIOR\t' + '\t'.join(('%.6f' % prob)[2:] for prob in get_priors())
	print 'total correct: %d = %.1f%%  (randomly expects %.1f correct)' % (total_correct, 100 * float(total_correct) / len(samples), len(samples) / len(out_options))


def train_on_data(root, samples):
	for node in breadth_first(root):
		if node.classifier is not None:
			if len(node.classifier.options) > 1:
				if VERBOSITY >= 1:
					print 'training', node
				#todo: this always trains on all samples, not correct?
				node.classifier.train(samples = samples)
				node.classifier.save()


if __name__ == '__main__':
	ready_go()


