
from numpy import abs, clip
from numpy.random import randn
from matplotlib.pyplot import subplots, show
from species.read_tree import get_root
from data.load import get_training_data
from species.output import get_output_options, prepare_output_info


def correct_for_priors(predictions, limit = 10):
	"""
		:param predictions: An ndarray with classes as columns and samples as rows. Each row should contain normalized probabilities. Classes should be the same order as get_priors.
		:return: The data with columns rescaled such that the average probability is close to the priors.
	"""
	assert predictions.shape[0] > 0 and predictions.shape[1] > 0
	output_info = prepare_output_info()
	priors = output_info.values()
	averages = clip(predictions.mean(1), 1e-5, 1)
	corrections = clip(priors / averages, 1. / limit, limit)
	rescaled = (predictions.T * corrections).T
	rescaled /= rescaled.sum(0)
	return rescaled


if __name__ == '__main__':
	root = get_root()
	samples = sum(get_training_data().values(), [])
	options = get_output_options()
	predictions = abs(randn(len(options), len(samples)))
	predictions /= predictions.sum(0)
	corrected = correct_for_priors(predictions)
	fig, (ax1, ax2) = subplots(1, 2, figsize = (6, 10))
	ax1.imshow(predictions[:20, :50].T)
	ax2.imshow(corrected[:20, :50].T)
	show()


