
from sys import argv
from argparse import ArgumentParser


RAW_TRAINING_IMAGES_DIR = 'data/train/'
RAW_TESTING_IMAGES_DIR = 'data/test/'

PROCESSED_TRAINING_IMAGES_DIR = 'data/bean/'
PROCESSED_TESTING_IMAGES_DIR = 'data/slo/'

TRAINING_FEATURES_DIR = 'data/tor/'
TESTING_FEATURES_DIR = 'data/spin/'

CAFFE_CODE_DIR = 'caffe/'

TREE_FILE = 'species/tree/single_tree.tsv'
TREE_FILE = 'species/tree/testing_tree_flat.tsv'

CLASSIFIER_CONF_DIR = 'classifiers/params/'
CLASSIFIER_STATE_DIR = 'classifiers/state/'

def CLASSIFIERS(key):
	from nn_caffe.caffe_net import CaffeClassifier
	from classifiers.random_classifier import RandomClassifier
	return {
		'default': CaffeClassifier,
		'random': RandomClassifier,
		'neural': CaffeClassifier,
		'caffe': CaffeClassifier,
	}[key]

TRAINING_FRACTION = 0.8
VALIDATION_FRACTION = 0.2

EXTERNAL_FEATURES = True

"""
	These can be overriden in local config file

	(Try not to change them here, to prevent creating a lot of commits.)
"""
SAMPLING_SEED = 42
PROCESS_COUNT = 1
ALL_DATA_IN_MEMORY = True

try:
	from settings_local import *
except ImportError:
	print 'no settings_local.py settings file found'

"""
	Increase verbosity like -v, -vv or -vvv.
"""
parser = ArgumentParser()
parser.add_argument('-v', action = 'count', default = 0, dest = 'verbosity', help = 'Add up to three -v args to increase verbosity.')
args = parser.parse_known_args(argv[1:])[0]
VERBOSITY = args.verbosity


