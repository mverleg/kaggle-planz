
from collections import OrderedDict
from run.cache import cache_noarg
from species.read_tree import get_root
from species.tree_node import TreeNode


@cache_noarg
def prepare_output_info():
	"""
		Get a list of the output nodes in deterministic order and in the expected format, skipping any that are not in the species tree.
	"""
	from settings import VERBOSITY
	if not len(TreeNode.nodes) > 1:
		raise Exception('Load the species tree before getting the expected output with get_output_options.')
	with open('species/output_info.txt', 'r') as fh:
		text = fh.read()
	out_opts = OrderedDict()
	loaded, skipped = 0, 0
	for line in text.splitlines():
		try:
			class_name, count = line.split()
		except ValueError:
			print 'skip', line
			continue
		try:
			out_opts[TreeNode.nodes[class_name]] = float(count)
		except KeyError:
			skipped += 1
			if VERBOSITY >= 3:
				print 'expected output "%s" not in tree' % class_name
		else:
			loaded += 1
			if VERBOSITY >= 3:
				print 'found expected output "%s" in tree' % class_name
	if VERBOSITY >= 2:
		print 'expected outputs order: loaded %d, skipped %d' % (loaded, skipped)
	norm = sum(out_opts.values())
	for opt in out_opts.keys():
		out_opts[opt] /= norm
	return out_opts


def get_output_options():
	return prepare_output_info().keys()


def get_priors():
	return prepare_output_info().values()


if __name__ == '__main__':
	root = get_root()
	for opt in get_output_options():
		print opt


