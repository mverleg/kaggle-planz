
from csv import reader
from settings import TREE_FILE
from species.tree_node import TreeNode


def get_root(tree_file = TREE_FILE):
	"""
		Build the entire tree.

		:param tree_file: Name of the file that defines the tree.
		:return: Root element of the tree.
	"""
	try:
		get_root._ROOT_CACHE
	except AttributeError:
		with open(tree_file) as tsvfile:
			tsvreader = reader(tsvfile, delimiter = '\t')
			for line in tsvreader:
				assert ' ' not in line, 'tree file should use tabs, it should not contain spaces'
				if len(line) >= 2:
					TreeNode(line[0], line[1])
		TreeNode.upgrade()
		get_root._ROOT_CACHE = TreeNode.nodes['root']
	return get_root._ROOT_CACHE


def breadth_first(root):
	"""
		This iterator traverses the entire tree starting at root breadth-first.
	"""
	yield root
	level = [root]
	while len(level):
		level = sum([node.children for node in level], [])
		for item in level:
			yield item


if __name__ == '__main__':
	get_root().disp()


