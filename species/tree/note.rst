
Notes about the tree file:

* the tree file should contain a line for each species or category
* the format it: species[tab]parent
* changing the order of lines in the tree file may mess with previously trained classifiers
* entries that start with a * are categories (so they don't have images)
* entries that start with a _ are not in the species chart
* subspecies are not allowed, make sure the parent of each species is a category


