
from sys import stderr
from os import listdir
from os.path import join, basename
from data.sample import KnownSample
from run.cache import cache_self
from classifiers.get_classifier import match_classifier
from settings import PROCESSED_TRAINING_IMAGES_DIR, TRAINING_FEATURES_DIR


class TreeNode(object):
	"""
		TreeNode is a node in the species graph: either a category or a species.

		* TreeNode.nodes is a dictionary name:node of all nodes.
		* node.name is the name (also directory name for species).
		* node.is_species indicates whether this node a species.
		* node.parent contains the parent node after TreeNode.upgrade() is called.
		* node.children contains the child nodes after TreeNode.upgrade() is called.
		* node.root is True for the root element, False otherwise.
		* node.chooser is the chooser instance that will decide between children of this class
		* node.samples are the samples created from node.images
		* node.descendant_samples are the samples created from node.descendant_images

		None of these properties should be changed directly.
	"""
	nodes = {}

	def __init__(self, name, parent = '-'):
		"""
			:param name: Name should be the name of the node. If it starts with * it is assumed to be a category, otherwise it should also be the directory name that contains the images.
			:param parent: Name of the parent node, or '-' if it is a top level node.
		"""
		if name.startswith('*'):
			self.name = name[1:]
			self.is_species = False
		else:
			self.name = name
			self.is_species = True
		self._parent_name = parent
		self.children = []
		self.root = False
		self.classifier = None
		TreeNode.nodes[self.name] = self

	@classmethod
	def upgrade(cls):
		"""
			Call once after building the entire species

			* upgrade parent names to node objects
			* all list of children to each node
		"""
		for node in cls.nodes.values():
			if node._parent_name == '-':
				node.parent = root
			else:
				try:
					node.parent = cls.nodes[node._parent_name]
				except:
					stderr.write('no node with name "%s" was found for node "%s" while calling TreeNode.upgrade()' % (node._parent_name, node.name))
			if not node.root:
				node.parent.children.append(node)
		for node in cls.nodes.values():
			if node.children and node.is_species:
				raise AssertionError('please redesign the tree to not have subspecies (e.g. make them subspecies siblings of their parents)')
			node.classifier = match_classifier(node)

	def __repr__(self):
		if self.classifier:
			return '%s [%s]' % (self.name, self.classifier)
		return self.name

	def disp(self, depth = 0):
		"""
			Display as a text-tree all the species of which the current node is the root.
		"""
		print '{0:s}{1:s}'.format('.  ' * depth, self.name)
		for node in self.children:
			node.disp(depth = depth + 1)

	@property
	def siblings(self):
		return self.parent.children

	#@property
	#def sibling_index(self):
	#	print [sib is self for k, sib in enumerate(self.siblings)]
	#	return [k for k, sib in enumerate(self.siblings) if self.name is sib.name][0]

	@property
	def images_dir(self):
		"""
			:return: The directory for the current node if there is one
			:raise AssertionError: if the current node is not a species (.is_species) but a category
		"""
		assert self.is_species, '%s is a category, not a species, so it doesn\'t have any images' % self.name
		pth = join(PROCESSED_TRAINING_IMAGES_DIR, self.name)
		return pth

	#@property
	#def features_dir(self):
	#	assert self.is_species, '%s is a category, not a species, so it doesn\'t have any features' % self.name
	#	_pth = join(TRAINING_FEATURES_DIR, self.name)
	#	return _pth

	@property
	def samples(self):
		if not self.is_species:
			return []
		images = [join(self.images_dir, pth) for pth in listdir(self.images_dir)]
		features = [join(TRAINING_FEATURES_DIR, '{0}_all.png'.format(basename(image).split('m')[0])) for image in images]
		return [KnownSample(imagepath, featurepath, self) for imagepath, featurepath in zip(images, features)]

	@property
	def descendants(self):
		"""
			:return: All child nodes.

			This INCLUDES the current node, which is always at the first position
			(so use .descendants[1:] if you don't want it)
		"""
		descs = [self]
		for node in self.children:
			descs.extend(node.descendants)
		return descs

	@property
	@cache_self
	def descendant_samples(self):
		"""
			REturns all samples for this node and all it's descendants.
		"""
		samples = []
		for node in self.descendants:
			samples.extend(node.samples)
		return samples


root = TreeNode('*root', '-')
root.root = True


